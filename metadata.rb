name             'arz-artifactory'
maintainer       'ARZ Allgemeines Rechenzentrum GmbH'
maintainer_email 'chrisitan.bitschnau@arz.at'
license          'All rights reserved'
description      'Installs/Configures arz-artifactory'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.2'

depends	          'artifactory'

